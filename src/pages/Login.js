import { Row, Col, Container, Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';



export default function Login(){

	// Allows us to consume the User Context object and it's properties to use for user validation
	const { user, setUser } = useContext(UserContext);	
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState("true")



	function authenticate(e) {

		e.preventDefault();

		//to send request in the server and load the received response
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			// We will received either a token or an error response
			console.log(data)

			// If no user information is found, the 'access' property will not be available and will return undefiend
			if(typeof data.access !== "undefined"){

				// The JWT will be used to retrieved user information acress the whole frontend application and string it in the localstorage

				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Chat App!'
				})
				setEmail("");
				setPassword("");
				
			} else {

				Swal.fire({
					title: "Authentication Failed!",
					icon: 'error',
					text: 'Please, check you login details and try again!'
				})
				setEmail("");
				setPassword("");
			}
		});
	};

	const retrieveUserDetails = (token) => {

		// put token to follow implementaion standards for JWTs
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Global user state for validation acress the whole app
			setUser({
				id: data._id,
				isAdmin: data.isAdmin,
				firstName: data.firstName,
				lastName: data.lastName
			});
		});
	};


useEffect(() => {

	if(email !== "" && password !== ""){

			setIsActive(true)

		}else {

			setIsActive(false)

		}

	}, [email, password]);


	return (
		(user.firstName !== null) ? 
			<Navigate to="/chat" />
		:
		<Container>
		 <Row className="login">
			 <h2 className="logo-login text-center mt-4">Login</h2>
			<Form onSubmit={(e) => authenticate(e)}>
				<Row className="mb-3">
        			<Form.Group as={Col} md="12" controlId="userEmail">
						<Form.Label>Email Address</Form.Label>
							<Form.Control
								type="email"
								placeholder="Enter email here"
								value={email}
								onChange={(e) => setEmail(e.target.value)}
								required
							    />
						    </Form.Group>
						</Row>

						<Row className="mb-3">
		        			<Form.Group as={Col} md="12"  controlId="password">
								<Form.Label>Password</Form.Label>
									<Form.Control
										type="password"
										placeholder="Password"
										value={password}
										onChange={(e) => setPassword(e.target.value)}
										required
										/>
									</Form.Group>
					   			</Row>
					   			
							{ isActive ? 

							<Button  variant="primary mt-3 w-100" size="sm" type="submit" id="submitBtn">Submit</Button>

						: 

					<Button  variant="primary mt-3 w-100" size="sm" type="submit" id="submitBtn" disabled>Submit</Button>

				}	
			</Form>
			<p className="mt-5 text-center">Not a member? <a href="Register">Register</a></p>
		 </Row>
		</Container>
	)
}