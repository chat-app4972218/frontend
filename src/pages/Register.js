import { Container, Form, Button, Row, Col} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';




export default function Register() {

	const {user} = useContext(UserContext);

	// State hooks to store values of the input fields
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	const Navigate = useNavigate();


    const registerUser = (e) => {
          e.preventDefault();
   
   fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password : password
		
		})
	})
   .then(res => res.json())
	.then(data => {
		console.log(data);

		if (data === true) {
			Swal.fire({
				title: "Registration successful",
				icon: "success",
				text: "Chat App!"
			})

		setFirstName("");
   		setLastName("");
   		setEmail("");
   		setPassword("");
		Navigate("/");

		} else {
			Swal.fire({
				title: "Duplicate email found",
				icon: "error",
				text: "Please provide another email"
			})
		setFirstName("");
   		setLastName("");
   		setEmail("");
   		setPassword("");

		}
	})
 }



	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match

		if((email !== "" && password !== "")){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);
	
	return (
        (user.id !== null) ?
        <Navigate to ="/" />

        :
       <Container className="register">
        <h2 className="mb-5" style={{marginLeft: 350}}>Register</h2>
        
        <Form onSubmit={registerUser}>
        <Row>
	        <Form.Group as={Col} md="4" controlId="userFirstName" className="my-2">
		        <Form.Label>First Name</Form.Label>
			        <Form.Control 
				        type="text" 
				        placeholder="Enter First Name"
				        value = {firstName}
				        onChange = {e => setFirstName(e.target.value)} 
				        required
			            /> 
	                </Form.Group>

			        <Form.Group as={Col} md="4" controlId="userLastName" className="my-2">
			        	<Form.Label>Last Name</Form.Label>
					        <Form.Control 
						        type="text" 
						        placeholder="Enter Last Name"
						        value = {lastName}
						        onChange = {e => setLastName(e.target.value)} 
						        required
						    /> 
			        	</Form.Group>
        			</Row>

        			 <Row>
			        <Form.Group as={Col} md="4" controlId="userEmail" className="my-2">
				        <Form.Label>Email address</Form.Label>
					        <Form.Control 
						        type="email" 
						        placeholder="Enter email"
						        value = {email}
						        onChange = {e => setEmail(e.target.value)} 
						        required
						        />
			        </Form.Group>

			        <Form.Group as={Col} md="4" controlId="password" className="my-2">
				        <Form.Label>Password</Form.Label>
					        <Form.Control 
						        type="password" 
						        placeholder="Password"
						        value = {password}
						        onChange = {e => setPassword(e.target.value)}
						        required
		       					 />
			        </Form.Group>
			       </Row>
        		 <Row>
			       	<Col xs lg="8">
			        { isActive ?
			        <Button variant="primary mt-3 w-100" size="sm" type="submit" id="submitBtn" >
			        Submit
			        </Button>
			        :
			        <Button variant="primary mt-3 w-100" size="sm" disabled type="submit" id="submitBtn">
			        Submit
			        </Button>

			    	}
			    	<p className="text-center mt-5">Already member? <a href="/">Login</a></p> 	
        	    </Col>
        	 </Row>
		</Form>
    </Container>	
    
	)
}