import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';

import UserContext from '../UserContext';

export default function Logout(){
	
	//it to access the user state and unsetUser function from the context provider
	const { unsetUser, setUser } = useContext(UserContext);

	// Clear the local storage of the user's information
	unsetUser();

	//this will allow the Logout to render first before triggering the useEffect which changes the state of our user
	useEffect(() => {
		setUser({
		   id: null,
           isAdmin: null,
           firstName: null,
           lastName: null
		});
	})

	return(
		<Navigate to="/" />
	)
}