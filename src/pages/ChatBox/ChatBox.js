import Style from './ChatBox.module.scss';
import { useEffect, useRef, useState } from 'react';
import { io } from "socket.io-client";

function ChatBox({ user }) {
  const socketConnection = useRef(null);
  const textAreaRef = useRef(null);
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    if (!socketConnection.current) {
      socketConnection.current = io('http://localhost:3001');
      socketConnection.current.on('connect', () => {
        
      })
      socketConnection.current.on('broadcast', (data) => {
        setMessages(prev => {
          return [
            ...prev,
            data
          ]
        })
      })
    }
  }, [user])


  const onClick = () => {
    socketConnection.current.emit("receive-chat", {user, message: textAreaRef.current.value})
  }
  return (
    <div className={Style.Main}>
      {/* Refactor to a different component, the messages */}
      <div className={Style.messagesContainer}>
        {messages.map((message, id) => {
          return (
            <div key={`${message.user.id}-${id}`} className={message.user.id === user.id ? Style.bySelf : Style.fromSender}>
              <p>Sender: {message.user.firstName}</p>
              <p>{message.message}</p>
            </div>
          )
        })}
      </div>
      <textarea
        ref={textAreaRef}
        placeholder="Send a Message"
        name=""
        id=""
        cols="30"
        resize="false">
      </textarea>
      <div className={Style.buttonContainer}>
        <button onClick={onClick}>Submit</button>
      </div>
    </div>
  )
}

export default ChatBox;
