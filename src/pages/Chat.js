import { Col, Row, Tab, Button, Container, Nav, Navbar, NavDropdown, Card } from 'react-bootstrap';
import ChatBox from './ChatBox/ChatBox';
import { Navigate, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import { useCallback, useContext, useState, useEffect } from 'react';
  
export default function Chat() {
  const { user } = useContext(UserContext);
  const [isActive, setIsActive] = useState("false")
  const [allusers, setUsers] = useState([])
  const usersData = useCallback(() => {
    return fetch(`${process.env.REACT_APP_API_URL}/users/client`)
      .then(res => res.json())
      .then((data) => setUsers(data));
  }, [])

  useEffect(() => {
    usersData();
    document.title = `Chat App | ${user.firstName} ${user.lastName}`
  }, [usersData, user])

  return (
    (user.firstName === null) ?
     <Navigate to="/" />
     :
    <Container>
      <Row style={{marginBottom : 200}}>
        <Col xs="12" className="mt-5">
          <h1 className="mb-5">
            <Card.Img
              className="mx-3"
              style={{ width: 20, height: '1rem' }}
              variant="top"
              src="https://res.cloudinary.com/dzsskdjr1/image/upload/v1682907398/online_siyvp6.png"
            />
            {user.firstName} {user.lastName}
          </h1>
          <hr/>
        </Col>
        <Col>
          <ChatBox user={user} />
        </Col>
      
         <Tab.Container id="left-tabs-example" defaultActiveKey="first">
          <Row>
            <Col sm={3}>
              <Nav variant="pills" className="flex-column"> 
              {allusers.map((prod) => (
                <Nav.Link key={prod._id} eventKey={prod._id}>
                {prod.firstName} {prod.lastName}
                </Nav.Link>
                ))}
               <hr />
               <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
              </Nav>
            </Col>
          </Row>
        </Tab.Container>
      </Row>
    </Container>
  );
}
