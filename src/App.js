import Chat from './pages/Chat'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import './App.css';

import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';


function App() {

  // for the user that's defined here for a global scope
  const [user, setUser] = useState([]);

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();

  }

  useEffect(() => { 
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
       // User is logged in
      if(typeof data._id !== "undefined"){
        setUser({
           id: data._id,
           isAdmin: data.isAdmin,
           firstName: data.firstName,
           lastName: data.lastName
        })

        // User is logged out
      } else {
        setUser({
          id: null,
          isAdmin: null,
          firstName: null,
          lastName: null
        })
      }
      
    })
  }, []);

  //for a component to return multiple elements.
  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
        <Routes>
          <Route path="/" element={<Login/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="/Chat" element={<Chat/>} />
          <Route path="/logout" element={<Logout/>} />
        </Routes>
    </Router>
    </UserProvider>
    </>
  );
}

export default App;